# coding=utf-8

import sys
import ast

def get_factors(x):
    factors = []
    for i in range(1,x):
        if x%i == 0:
            factors.append(i)
    return factors

def show_number_type(numbers):
    for i in numbers:
        s = sum(get_factors(i))
        if s < i:
            print i, 'es un número defectivo'
        else:
            if s == i:
                print i, 'es un número perfecto'
            else:
                print i, 'es un número abundante'

if __name__=='__main__':
    try:
        numbers = ast.literal_eval(sys.argv[1])
        all_good = all(
            n >= 0 and isinstance(n, int)
            for n in numbers
        )
        if all_good:
            show_number_type(numbers)
        else:
            raise ValueError
    except (ValueError, SyntaxError):
        print 'La lista de números tiene que contener números enteros positivos'
