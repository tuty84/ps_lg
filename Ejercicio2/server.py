# -*- coding: utf-8 -*-
from flask import Flask, jsonify, render_template
import urllib.request, json, datetime, re

# creates a Flask application, named app
app = Flask(__name__)

def get_json_object(url):
    with urllib.request.urlopen(url) as req:
        response = req.read()
        return json.loads(response.decode('utf-8'))

def process_serie1(json_object):
    for data in json_object:
        cat = data['cat'].upper()
        date_ms = data['d']
        date_str = datetime.datetime.fromtimestamp(float(date_ms)/1000).strftime('%Y-%m-%d')
        value = data['value']
        if date_str in categories[cat]:
            categories[cat][date_str] += value
        else:
            categories[cat][date_str] = value

def process_serie2(json_object):
    for data in json_object:
        cat = data['categ'].upper()
        date_str = data['myDate']
        value = data['val']
        if date_str in categories[cat]:
            categories[cat][date_str] += value
        else:
            categories[cat][date_str] = value

def process_serie3(json_object):
    for data in json_object:
        raw = data['raw']
        searchObj = re.search( r'(\d{4}-\d{2}-\d{2}).*#(CAT \d)#', raw, re.M|re.I)
        if searchObj:
               cat = searchObj.group(2).upper()
               date_str = searchObj.group(1)
               value = data['val']
               if date_str in categories[cat]:
                   categories[cat][date_str] += value
               else:
                   categories[cat][date_str] = value


# a route where we will display a welcome message via an HTML template
@app.route("/")
def index():
    return render_template('index.html')

@app.route("/get_data_series")
def get_data_series():
    global categories
    categories = {}
    categories['CAT 1'] = {}
    categories['CAT 2'] = {}
    categories['CAT 3'] = {}
    categories['CAT 4'] = {}
    json_object = get_json_object("http://s3.amazonaws.com/logtrust-static/test/test/data1.json")
    process_serie1(json_object)
    json_object = get_json_object("http://s3.amazonaws.com/logtrust-static/test/test/data2.json")
    process_serie2(json_object)
    json_object = get_json_object("http://s3.amazonaws.com/logtrust-static/test/test/data3.json")
    process_serie3(json_object)
    return jsonify(
        cat1=json.dumps(categories['CAT 1'], sort_keys = True),
        cat2=json.dumps(categories['CAT 2'], sort_keys = True),
        cat3=json.dumps(categories['CAT 3'], sort_keys = True),
        cat4=json.dumps(categories['CAT 4'], sort_keys = True))

# run the application
if __name__ == "__main__":
    app.run(debug=True)
