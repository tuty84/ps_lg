EJERCICIO1

- Go to the folder Ejercicio1 and execute the following command:

  python ex1.py [num1, ..., numN] (i.e: python ex1.py [1,2,3,6,1000])

EJERCICIO2

- Requirements:

  - Install Flask (http://flask.pocoo.org/)
  - Python 3

- Go to the folder Ejercicio2 and execute the following commands:

  $ export FLASK_APP=server.py
  $ flask run

  Now head over to http://127.0.0.1:5000/ and you should see the web application running

- Tested with:

  - Flask 1.0.2
  - Python 3.4.2
